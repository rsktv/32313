﻿#include <math.h>
#include <iostream>
#include "SomeFunctions.hpp"


int main()
{
	int n;
	int m;
	int matrix[10][10];
	int isprime[10];

	Read(n, m, matrix, isprime);

	int max = 0;
	int max2 = 0;

	Max(max, max2, matrix, n, m);

	IsPrime(n, m, matrix, isprime);

	if (max == max2)
	{
		Swap(isprime, matrix, n, m);
	}

	Write(n, m, matrix);
}