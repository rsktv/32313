#include "SomeFunctions.hpp"

void Read(int& n, int& m, int matrix[10][10], int isprime[10])
{
	std::cin >> n >> m;

	for (int i = 0; i < n; i++)
	{
		isprime[i] = 0;
		for (int j = 0; j < m; j++)
		{
			std::cin >> matrix[i][j];
		}
	}
}

bool Max(int& max, int& max2, int matrix[10][10], int n, int m)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (max < matrix[i][j])
			{
				max2 = max;
				max = matrix[i][j];
			}
			else
			{
				if (max2 < matrix[i][j])
				{
					max2 = matrix[i][j];
				}
			}
		}

	}
}

bool IsPrime(int n, int m, int matrix[10][10], int isprime[10])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			int b = 1;
			if (matrix[i][j] > 2)
			{
				for (int d = 2; d <= sqrt(matrix[i][j]); d++)
				{
					if (matrix[i][j] % d == 0)
					{
						b = 0;
						break;
					}
				}
				isprime[i] += b;
			}
		}
	}
}

bool Swap(int isprime[100], int matrix[10][10], int n, int m)
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int k = i + 1; k < n; k++)
		{
			if (isprime[i] > isprime[k])
			{
				for (int j = 0; j < m; j++)
				{
					std::swap(matrix[i][j], matrix[k][j]);
				}
				std::swap(isprime[i], isprime[k]);
			}
		}
	}
}

void Write(int n, int m, int matrix[10][10])
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			std::cout << matrix[i][j] << " ";
		}
		std::cout << "\n";
	}
}
