#pragma once
#include <iostream>
#include <math.h>


void Read(int& n, int& m, int matrix[10][10], int isprime[10]);


bool Max(int& max, int& max2, int matrix[10][10], int n, int m);


bool IsPrime(int n, int m, int matrix[10][10], int isprime[10]);


bool Swap(int isprime[100], int matrix[10][10], int n, int m);


void Write(int n, int m, int matrix[10][10]);

